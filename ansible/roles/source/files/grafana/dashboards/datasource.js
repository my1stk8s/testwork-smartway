{
        "name": "postgres-datasource",
        "type": "postgres",
        "typeLogoUrl": "public/app/plugins/datasource/postgres/img/postgresql_logo.svg",
        "access": "proxy",
        "url": "51.250.103.16:5432",
        "user": "postgres",
        "password": "postgres",
        "database": "postgres",
        "basicAuth": false,
        "isDefault": true,
        "uid": "ff7eadc5-1367-4661-8d19-b9bbdfb76160",
        "jsonData": {
            "sslmode": "disable"
        }
}
