# YandexCloud-TestWork-Smartway

Тестовое задание :

- Создание инфрастуктуры в облаке [YandexCloud](https://https://cloud.yandex.ru) с помощью инструмента `Terraform`;
- все приложения развернуты и настроены  с помощью [Ansible] и [Terraform]
- Настроен деплой приложений через [Docker-Compose],приложения:[Grafana] ,[Postgresql] 
## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [YandexCloud](https://https://cloud.yandex.ru)
2. Установите [YandexCloud CLI](https://cloud.yandex.ru/docs/cli/operations/install-cli) для управления аккаунтом и ресурсами
3. Далее необходимо получить [AuthToken](https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token)
4. [Cконфигурируйте YandexCloud CLI](https://cloud.yandex.ru/docs/cli/operations/authentication/user), используя раннее полученный `AuthToken`
5. В Папке проетка по пути /terraform подставьте значения в файл private.auto.tfvars.dist и переименуйте его private.auto.tfvars
6. В Папке проетка по пути /terraform подставьте значения вашего доменного имени в файл dns.tf
7. Купите домен у любого регистратора или зарегистрировать бесплатный, получить сертификат [Let's Encrypt](https://cloud.yandex.com/en/docs/certificate-manager/concepts/managed-certificate) и [прописать NS-сервера у регистратора домена](https://cloud.yandex.ru/docs/storage/operations/hosting/own-domain)
8. Установите [Terraform](https://www.terraform.io/downloads)
9. В Папке проетка по пути /terraform/instance  и  /terraform/ в файлах  variables.tf укажите путь до приватного ключа  
9. В Папке проетка по пути /terraform/instance  в файлe  meta.yml укажите свой публичный shh ключ для доступа на ВМ
## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов:

| Элемент | Назначение |
| --- | ----------- |
| [Yandex_compute_instance] | 1 инстанс. 2 cpu 4 ram
| [Yandex_vpc_network] | Необходимо для создания изолированной сетевой инфраструктуры |
| [Yandex_vpc_subnet] | Необходимы для разделения сетевых ресурсов на публичные и приватные. |

> Таблица 2.

1. Склонируйте репозиторий к себе для работы с ним:
   ```shell
   git clone https://gitlab.com/my1stk8s/testwork-smartway.git
   ```
> Таблица 3.

2. Инициализируйте Terraform и запустите создание ресурсов через баш скрипт в кореной дериктории с проектом, полая установка длиться около 7 мин:

   ```shell
    ./apply.sh
   ```
3. Для удаления всех рессурсов 
   ```shell
    ./destroy.sh
   ```
