# подключение к провайдеру и  и отправка стейта в хранилище
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.81.0"
    }
  }
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "s3bucketacc"
    region     = "ru-central1"
    key        = "terraform.tfstate"
    
    skip_region_validation      = true
    skip_credentials_validation = true
  } 
}
# токены
provider "yandex" {
  token     = var.YC_TOKEN
  cloud_id  = var.YC_CLOUD_ID
  folder_id = var.YC_FOLDER_ID
}
# создание сети
resource "yandex_vpc_network" "network-tf" {
  name = "network-tf"
}

resource "yandex_vpc_subnet" "subnet-tf-2" {
  name           = "subnet-tf-2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-tf.id
  v4_cidr_blocks = ["192.168.7.0/24"]
}
# поднятие  инстансов по  шаблону
module "instance_1" {
  source = "./instance"
  name   = "server"
  custom_subnet_id = yandex_vpc_subnet.subnet-tf-2.id
  instance_family_image = "ubuntu-2204-lts"
  zone_id = "ru-central1-b"
}


resource "local_file" "inventory" {
  filename = "../ansible/hosts"
  content = <<EOF
[server]
server ansible_host=${module.instance_1.external_ip_address_vm} ansible_user=ubuntu ansible_ssh_private_key_file=${var.ssh_key_private}

  EOF
depends_on = [
    module.instance_1
  ]
}
resource "local_file" "inventory1" {
  filename = "../ansible/roles/nginx/templates/nginx.conf.j2"
  content = <<EOF
server {
    listen 80;
    server_name gr.moskvitin.website;

    location / {
        proxy_pass http://${module.instance_1.external_ip_address_vm}:3000;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    # Дополнительные настройки могут быть добавлены по необходимости
}
  EOF
depends_on = [
    module.instance_1
  ]
}
resource "local_file" "inventory2" {
  filename = "../ansible/roles/source/files/grafana/dashboards/datasource.js" 
  content = <<EOF
{
        "name": "postgres-datasource",
        "type": "postgres",
        "typeLogoUrl": "public/app/plugins/datasource/postgres/img/postgresql_logo.svg",
        "access": "proxy",
        "url": "${module.instance_1.external_ip_address_vm}:5432",
        "user": "postgres",
        "password": "postgres",
        "database": "postgres",
        "basicAuth": false,
        "isDefault": true,
        "uid": "ff7eadc5-1367-4661-8d19-b9bbdfb76160",
        "jsonData": {
            "sslmode": "disable"
        }
}
  EOF
depends_on = [
    module.instance_1
  ]
}
resource "local_file" "inventory3" {
  filename = "../ansible/roles/source/files/grafana/dashboards/post.sh" 
  content = <<EOF
curl --user admin:foobar  -H 'Accept: application/json' -H 'Content-Type: application/json; charset=UTF-8' -X POST --data @datasource.js http://${module.instance_1.external_ip_address_vm}:3000/api/datasources 
echo datasource created
  EOF
depends_on = [
    module.instance_1
  ]
}

resource "null_resource" "ConfigureAnsibleLabelVariable" {
  provisioner "local-exec" {
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook  -i ../ansible//hosts ../ansible/playbook.yaml -vvv"
  }
  depends_on = [
    local_file.inventory
  ]
}

