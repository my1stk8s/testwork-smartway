# создание зоны DNS и определение имен доменов высшего уровня 
resource "yandex_dns_zone" "moskvitin" {
  name        = "moskvitin-zone-name"
  description = "dns"

  labels = {
    label1 = "test-public"
  }

  zone    = "${var.dns_record}"
  public  = true
  private_networks = [yandex_vpc_network.network-tf.id]
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.moskvitin.id
  name    = "gr.${var.dns_record}"
  type    = "A"
  ttl     = 200
  data    = [module.instance_1.external_ip_address_vm]
}
